var grdeBesede = [];
$.get('/swearWords.txt', function(data){
  grdeBesede = data.split('\n');
  console.log(grdeBesede);
});

function filterGrdihBesed(text){
  for (var i=0;i<grdeBesede.length;i++){
    
  }
  return text;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo); //html namesto text
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo=zamenjajSmeske(sporocilo);
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function zamenjajSmeske(text) {
  var url = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/"; 
  text = text.replace(/</g, "&lt");
  text = text.replace(/;\)/g, "<img src='"+url+"wink.png'>");
  text = text.replace(/:\)/g, "<img src='"+url+"smiley.png'>");
  text = text.replace(/\(y\)/g, "<img src='"+url+"like.png'>");
  text = text.replace(/:\*/g, "<img src='"+url+"kiss.png'>");
  text = text.replace(/:\(/g, "<img src='"+url+"sad.png'>");
  return text;
}


var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var vzdevek = "";
  var kanal = "";

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      vzdevek = rezultat.vzdevek;
      $('#kanal').text(rezultat.vzdevek+" @ "+kanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    kanal = rezultat.kanal;
    $('#kanal').text(vzdevek+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
//<<<<<<< HEAD
});
//<<<<<<< HEAD

//Saved working directory and index state WIP on Naloga_2_1: 08f0da2 README.md edited online with Bitbucket HEAD is now at 08f0da2 README.md edited online with Bitbucket
//=======
//
//>>>>>>> Naloga_2_3
//=======
//});//
//>>>>>>> Naloga_2_4
